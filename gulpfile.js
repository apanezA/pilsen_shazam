'use strict';

var gulp       = require('gulp'),
    jade       = require('gulp-jade'),
    browserify = require('browserify'),
    source     = require('vinyl-source-stream'),
    buffer     = require('vinyl-buffer'),
    uglify     = require('gulp-uglify'),
    gutil      = require('gulp-util'),
    sass       = require('gulp-sass'),
    connect    = require('gulp-connect'),
    babel      = require('gulp-babel'),
    jshint     = require('gulp-jshint'),
    cache      = require('gulp-cache'),
    size       = require('gulp-size'),
    sourcemaps = require('gulp-sourcemaps'),
    rename     = require("gulp-rename"),
    plumber    = require('gulp-plumber'),

    gulpif = require('gulp-if');

var env = process.env.NODE_ENV || 'development',
    outputDir = 'dist',
    outputDirTemplate = outputDir;


gulp.task('templates', function () {
    var local = {};

    return gulp.src('source/layouts/*.jade')
        .pipe( plumber() )
        .pipe( jade({
            locals: local,
            pretty: env === 'development'
        }) )
        .pipe( gulp.dest(outputDirTemplate) )
        .pipe( connect.reload() );
});


gulp.task('ES6', function () {
    return gulp.src('source/ES6/**/*.js')
        .pipe( plumber() )
        .pipe( babel() )
        .pipe( jshint() )
        .pipe( jshint.reporter( require('jshint-stylish') ) )
        .pipe( gulp.dest('source/javascript') )
        .pipe( connect.reload() );
});


gulp.task('js', function() {  
    return browserify('source/javascript/application.js')
        .bundle()
        .pipe( source('bundle.js') )
        .pipe( buffer() )        
        .pipe( uglify() )
        //.pipe( gulp.dest('../static/js') )
        .pipe( gulp.dest( outputDir + '/js') )
        .pipe( connect.reload() );
});


gulp.task('styles', function () {
    var config = {
        outputStyle : 'compressed'
    };

    return gulp.src('source/sass/style.scss')
        .pipe( plumber() )
        .pipe( sass() )
        .pipe( gulp.dest(outputDir + '/css') )
        .pipe( gulp.dest('../static/css') )
        .pipe( sass(config) )
        .pipe( rename('style.min.css') )
        .pipe( gulp.dest(outputDir + '/css') )        
        .pipe( connect.reload() );
});


gulp.task('watch', function () {
    gulp.watch( 'source/layouts/**/*.jade', ['templates'] );
    //gulp.watch( 'source/ES6/**/*.js', ['ES6'] );
    gulp.watch( 'source/javascript/**/*.js', ['js'] );
    gulp.watch( 'source/sass/**/*.scss', ['styles'] );
});


gulp.task('connect-server', function () {
    connect.server({
        root : outputDir,
        port : 7000,
        livereload : true
    });
});


gulp.task('build', ['templates', 'js', 'styles']);

gulp.task('default', ['templates', 'js', 'styles', 'watch', 'connect-server'] );
