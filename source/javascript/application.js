"use strict";
var validateFormLogin  = require('./lib/validateFormLogin.js'),
    validateFormRegister = require('./lib/validateFormRegister.js'),
    Ubigeo = require('./lib/ubigeo.js');


$(document).on('ready', function () {

    /* Menu Action */
    $("#menu-btn .menu_btn").on('click', function () {
        $("nav.menu").removeClass("close");
        setTimeout(function () {
            $("nav.menu .menu__items").addClass("opacity-show");
        }, 200);
    });

    $(".menu .menu__close").on('click', function () {
        $("nav.menu .menu__items").removeClass("opacity-show");
        setTimeout(function () {
            $("nav.menu").addClass("close");
        }, 200);
    });


    /* Register */
    var openRegister = function openRegister() {
        $('aside#register').fadeIn();
        $('#day-born').focus();
    };

    var closeRegister = function closeRegister() {
        $( "aside#register" ).fadeOut(250, function() {

            $(".respond__success, .respond__wrong, .register-form").fadeOut(0);
            $("#login-form").fadeIn(0);
            $("#register .register__title h2").removeClass("respond");
        });

    };

    $( ".btn-action--register, .openRegister" ).on('click', function (e) {
        openRegister();
    });

    $( "#register .register__close" ).on('click', function () {
        closeRegister();
    });


    /* Acordeon */
    $('.preguntas__list > dt, .ganadores__list > dt').click(function () {
        var obj = $(this);

        $('.preguntas__list > dt .icon, .ganadores__list > dt .icon').addClass("plus");
        $(".icon", obj).removeClass("plus");

        obj.next().slideDown(450).siblings(".anwser").slideUp(350);        
    });
    

    /*  Compartir Social  */
    var fbShare = function( video, title, text ) {
        FB.ui({
          method: 'feed',
          link: "https://www.youtube.com/watch?v=" + video,
          caption: title,
          description: text,
          picture: "http://img.youtube.com/vi/" + video + "/hqdefault.jpg"
        }, function(response) {
        });
    };

    /* Configuración popup para Compartir en Twitter */
    var twShare = function(text, video) {
        var idVideo  = encodeURI("https://www.youtube.com/watch?v=" + video),
            phrase = encodeURI(text),
            uri = 'https://twitter.com/share?url=' + video + '&text=' + phrase + '%23Visitaatupata&display=popup';
        /* GA Event */
        //ga('send', 'event', 'app_fb_s6', 'app_general', 'btn_compartirappTw');
        window.open(uri, "Visita a tu pata - Pilsen Callao", "status = 1, height = 450, width = 620, resizable = 0");        
    };

    $(".videos__item__social li a").on('click', function() {
        var social = $(this).data("social"),
            video  = $(this).data("video"),
            title  = $(this).data("title"),
            text   = $(this).data("text");
        
        if ( social === "fb" ) {
            fbShare(video, title, text);
        } else {
            twShare(text, video);
        }
    });


    /* Formulario */
    $(".register-form__field--check input").change(function() {
        var self = $(this);
        self.parent().toggleClass("check");
    });

    $(".register-form__group--genre input").change(function() {
        var self = $(this);
        $(".register-form__group--genre .wrap").removeClass("check");
        self.parent().addClass("check");
    });

    /* Rellenar Formulario */
    var player = {}; /* Object Player */

    var fillFormRegister = function() {
        // Default values
        $("#dni_log").val( $("#dni").val() );
        $("#day").val( $("#day_born").val() );
        $("#month").val( $("#month_born").val() );
        $("#year").val( $("#year_born").val() );

        // each object player
        $.each( player, function( key, value ) {
            if ( value !== "" && value !== "None" && value !== "(Nada)" ) {

                if ( key === "gender" ) {
                    var genre = value.toUpperCase();               
                    $('.register-form__group--genre input[value="' + genre + '"]').prop("checked", true).change();                    

                } else if ( key === "province" || key === "department" || key === "district" ) {
                    setTimeout(function() {
                      $('#'+key+' option[value="'+value+'"]').attr('selected', 'selected').change();
                      //$('#'+key).prop("readonly", true);
                    }, 700);

                } else {
                    //$("#"+key).val(value).prop("readonly", true);
                    $("#"+key).val(value);
                }
            } else {

            }
        });
    };

    /* Aparecer Form de registrado */
    var appearForm = function() {
        $(".login-form").fadeOut(300);
        $(".register-form").delay(320).fadeIn(300);
        fillFormRegister();
    };

    /* Mesanje de usuario ya registrado */
    var appearWrongMsg = function() {
        $(".login-form").fadeOut(300);        
        $("#register .respond__wrong").delay(320).fadeIn(300);
        $("#register .register__title h2").addClass("respond").text("¿QUÉ, OTRA VEZ?");
    };

    /* Mesanje de registro exitoso */
    var appearSuccessMsg = function() {
        $(".register-form").fadeOut(300);        
        $("#register .respond__success").delay(320).fadeIn(300);
        $("#register .register__title h2").addClass("respond").text("ANDA HACIENDO MALETAS");
    };


    /* Validacion de Formularios */
    $('.login-form .login-form__field input, #mobile').numeric();
    $('#name, #first-surname, #last-surname').alpha({allow:" "});

    var viewFormValidateLogin    = $('#login-form').validate( validateFormLogin ),
        viewFormValidateRegister = $('#register-form').validate( validateFormRegister );

    /* Ubigeo Select */
    var ubigeo = new Ubigeo();

    ubigeo.callToAjax('', '#department', '• Departamento');

    $('#department').on('change', function() {
        var code = $(this).val();
        ubigeo.callToAjax(code, '#province', '• Provincia');
        $('#district').empty();
        $('#district').append('<option value="">• Distrito</option>');
    });

    $('#province').on('change', function() {
        var code = $(this).val();
        ubigeo.callToAjax(code, '#district', '• Distrito');
    });


    /* Eventos Submit */
    $('#btn-login').on('click', function(e) {
        e.preventDefault();
        var self = $(this),
            birthDate = $('#day_born').val() +  "/" + $('#month_born').val() +  "/" + $('#year_born').val();
        
        self.prop('disabled', 'disabled');
     
        if ( $('#login-form').valid() )  {
            $.ajax({
                type: "POST",
                url: '/validate',
                data: JSON.stringify({
                  'dni': $('#dni').val(),
                  'birth_date': birthDate
                }),
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if ( data.success )  {
                        player = data.player; /* Object Player */
                        appearForm();
                    } else {
                        self.removeProp('disabled');
                        if ( data.errors['old'] ) {
                            $("#errors").append("<em>La fecha no coincide con el DNI registrado</em>");                            
                        } else {
                            appearWrongMsg();
                        }
                    }
                }
            });
        }
        else {
            self.removeProp('disabled');
        }
    });

    $('#btn-register').on('click', function(e) { 
        e.preventDefault();
        var self = $(this),
            birthDate = $('#day_born').val() +  "/" + $('#month_born').val() +  "/" + $('#year_born').val();

        self.prop('disabled', 'disabled');
     
        if ( $('#register-form').valid() )  {
            $.ajax({
                type: "POST",
                url: '/register',
                data: JSON.stringify({
                    'name': $('#name').val(),
                    'lastname_father': $('#lastname_father').val(),
                    'lastname_mother': $('#lastname_mother').val(),
                    'gender': $('input[name="gender"]:checked').val(),
                    'email': $('#email').val(),
                    'birth_date': birthDate,
                    'dni': $('#dni').val(),
                    'department': $('#department').val(),
                    'province': $('#province').val(),
                    'district': $('#district').val(),                
                    'mobile': $('#mobile').val(),
                    'newsletter' : $('#promo').val()              
                }),
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    if ( data.success )  {
                        appearSuccessMsg();
                    } else {                        
                        alert('Hubo un error al procesar el registro');
                    }
                }
            });
        } else {
            //$('.maincontent-form .btn-yanbal-start-run').removeProp('disabled');            
            self.removeProp('disabled');
        }
    });

    /* eventos para los campos de fecha de nacimiento */
    $('#year_born, #month_born, #day_born').on('keyup', function(){ 
        $('input#birth_date').val( [ $('#year_born').val(), $('#month_born').val(), $('#day_born').val() ].join('/') );
        if ( $('input#birth_date').val() !== '' && $('input#birth_date').val() !== '//' ){
            viewFormValidateLogin.element('input#birth_date');
        }
    });
}); 

