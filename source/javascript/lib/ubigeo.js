var Ubigeo = function() {
    var self = this;
    this.fillOptions = function(data, id, title) {
        var arrayHTML, i, item, len;
        arrayHTML = [];
        $(id).empty();
        $(id).append('<option value="">' + title + '</option>');
        
        i = 0;
        len = data.length;
        while (i < len) {
            item = '<option value="' + data[i]['id'] + '">' + data[i]['name'] + '</option>';
            arrayHTML.push(item);
            i++;
        }
        return $(id).append(arrayHTML);
    };

    this.callToAjax = function(code, id, title) {
        $.ajax({
            type: 'GET',
            url: '/sabmiller/ubigeo?parent='+code,
            success: function(data) {
                self.fillOptions(data.ubigeos, id, title);
            },
            dataType: 'JSON'
        });
    };
};

module.exports = Ubigeo;